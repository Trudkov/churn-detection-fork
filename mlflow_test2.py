import os
from random import random, randint
from mlflow import log_artifacts, log_param, log_metric, log_artifact
import mlflow
import boto3

# session = boto3.Session(
#          aws_access_key_id='minioadmin',
#          aws_secret_access_key='minioadmin')
#
# s3 = boto3.client('s3')


#os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
#os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"
#os.environ["MINIO_ROOT_USER"] = "minioadmin"
#os.environ["MINIO_ROOT_PASSWORD"] = "minioadmin"
#os.environ['AWS_S3_BUCKET'] ='arts'
os.environ["MLFLOW_S3_ENDPOINT_URL"] = f"http://45.95.234.3:9000"
os.environ['MLflow_TRACKING_USERNAME'] = 'MLflow-user'

#mlflow.set_tracking_uri("http://185.178.47.6:5000")
#mlflow.set_tracking_uri('http://127.0.0.1:5000')
mlflow.set_tracking_uri('http://45.95.234.3:5000')
mlflow.set_experiment('mlflow_test')
#os.environ["MLFLOW_S3_ENDPOINT_URL"] = f"http://45.95.234.3:9000"
#os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
#os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"
#os.environ['MLFLOW_S3_ENDPOINT_URL'] = "http://45.95.234.3:9000"
#os.environ['MLFLOW_TRACKING_URI'] = "http://45.95.234.3:5000"
# ;AWS_ACCESS_KEY_ID=minioadmin;AWS_SECRET_ACCESS_KEY=minioadmin;AWS_DEFAULT_REGION=us-west-2;AWS_S3_BUCKET=box;MLFLOW_TRACKING_URI=http://80.90.189.115:5000;MLFLOW_S3_ENDPOINT_URL=http://80.90.189.115:9001


if __name__ == '__main__':
    log_param("param1", randint(0, 100))
    # log metrics by changiung
    log_metric("foo", random())
    log_metric("foo", random() + 1)
    log_metric("foo", random() + 2)

    if not os.path.exists("outputs"):
        os.makedirs("outputs")

    # logiruem file
    with open("outputs/test.txt", "w") as f:
        f.write("hello world")
    #log_artifact("outputs/test.txt")
    log_artifacts("outputs")
