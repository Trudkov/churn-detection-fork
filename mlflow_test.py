import mlflow
import os
from mlflow.models.signature import infer_signature

from sklearn.model_selection import train_test_split
from sklearn.datasets import load_diabetes
from sklearn.ensemble import RandomForestRegressor


# mlflow.set_tracking_uri("http://185.178.47.6:5000")
# mlflow.set_experiment('mlflow_test8')
# os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://185.178.47.6:9000'
# os.environ['MLflow_TRACKING_USERNAME'] = 'MLflow-user'
#
#
#mlflow.set_tracking_uri('http://127.0.0.1:5000')
#mlflow.set_tracking_uri("http://80.90.189.115:5000")

mlflow.set_tracking_uri("http://45.95.234.3:5000")

# os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
# os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"
os.environ['MLFLOW_S3_ENDPOINT_URL'] = "http://45.95.234.3:9000"
#os.environ['MLFLOW_TRACKING_URI'] = "http://45.95.234.3:5000"
#os.environ['MLflow_TRACKING_USERNAME'] = 'MLflow-user'

mlflow.set_experiment('sklearn1')
mlflow.sklearn.autolog() # автолог сам выбирает flavour через mlflow.
with mlflow.start_run():
    db = load_diabetes()
    X_train, X_test, y_train, y_test = train_test_split(db.data, db.target)
    # Create and train models.
    rf = RandomForestRegressor(n_estimators=100, max_depth=6, max_features=3)
    rf.fit(X_train, y_train)

    # Use the model to make predictions on the test dataset.
    predictions = rf.predict(X_test)
    # signature пишутся вот так
    #signature = infer_signature(X_train, predictions)
    # model логируется вот так: модель, название и доп-но сигнатуры
    #mlflow.sklearn.log_model(rf, "diabetes_rf", signature=signature)

    autolog_run = mlflow.last_active_run()
