import glob
import click
import pandas as pd

from src.constants import INDEX_COL


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def load_data(input_path: str, output_path: str):
    """
    Загрузка данных, предоставленных об бизнеса, соединение в один датафрейм, сохранение
    @param input_path: путь к сырым данным
    @param output_path: путь к сведенным данным для сохранения
    @return: None
    """
    files = glob.glob(input_path + "*.csv")
    # соеденим данные в папках
    list_temp_df = []
    for file in files:
        temp_df = pd.read_csv(file).set_index(INDEX_COL)
        list_temp_df.append(temp_df)
    # сохраним сведенные данные

    merged_data = pd.concat(list_temp_df, axis=1).reset_index()
    merged_data.to_csv(output_path, index=False)


if __name__ == "__main__":
    load_data()
