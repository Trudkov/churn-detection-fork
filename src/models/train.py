# pylint: disable=invalid-name
import os
import pickle
import warnings
from collections import Counter

import click
import pandas as pd
from feature_engine.encoding import OneHotEncoder
from feature_engine.imputation import (
    AddMissingIndicator,
    CategoricalImputer,
    MeanMedianImputer,
)
from sklearn.pipeline import Pipeline
from xgboost import XGBClassifier

from src.models.metrics import metrics_calculation
from src.constants import (
    CATEGORICAL_VARIABLES,
    CATEGORICAL_VARIABLES_WITH_NA,
    NUMERICAL_VARIABLES,
    NUMERIC_VARIABLES_WITH_NA,
    RANDOM_STATE,
    TARGET,
)
from sklearn.metrics import (
    accuracy_score,
    classification_report,
    roc_auc_score,
)
warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)
import mlflow
import joblib
from mlflow.models.signature import infer_signature
from mlflow.tracking import MlflowClient

from dotenv import load_dotenv
load_dotenv()
remote_server_uri = os.getenv('MLFLOW_TRACKING_URI')

os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://45.95.234.3:9000'

@click.command()
@click.argument("train_data", type=click.Path(exists=True))
@click.argument("validation_data", type=click.Path(exists=True))
@click.argument("model_output", type=click.Path())
def train_model(
        train_data: pd.DataFrame,
        validation_data: pd.DataFrame,
        model_output: str
):
    """
    Создание пайплайна, тренировка модели, расчет метрик качества на
    валидационной выборке
    @param train_data:
    @param validation_data:
    @param model_output:
    @return:
    """
    with mlflow.start_run():

        # split data for train and validation
        train_data = pd.read_csv(train_data)
        validation_data = pd.read_csv(validation_data)

        X_train, X_val = (
            train_data.drop(TARGET, axis=1),
            validation_data.drop(TARGET, axis=1),
        )
        y_train, y_val = train_data[TARGET], validation_data[TARGET]
        # count for balance class
        counts = Counter(y_train)
        # set up the pipeline
        churn_pipe = Pipeline(
            [
                # ===== IMPUTATION =====
                # add missing indicator to numerical variables
                (
                    "missing_indicator_cat",
                    AddMissingIndicator(variables=CATEGORICAL_VARIABLES),
                ),
                # impute categorical variables with string 'missing'
                (
                    "categorical_imputation",
                    CategoricalImputer(
                        imputation_method="missing", variables=CATEGORICAL_VARIABLES_WITH_NA
                    ),
                ),
                # add missing indicator to numerical variables
                (
                    "missing_indicator_num",
                    AddMissingIndicator(variables=NUMERICAL_VARIABLES),
                ),
                # impute numerical variables with the median
                (
                    "median_imputation",
                    MeanMedianImputer(
                        imputation_method="median", variables=NUMERIC_VARIABLES_WITH_NA
                    ),
                ),
                # == CATEGORICAL ENCODING ======
                # encode categorical variables using one hot encoding into k-1 variables
                ("categorical_encoder", OneHotEncoder(variables=CATEGORICAL_VARIABLES)),
                # estimator
                (
                    "xgboost",
                    XGBClassifier(
                        nthread=8,
                        scale_pos_weight=counts[0] / counts[1],
                        random_state=RANDOM_STATE,
                    ),
                ),
            ]
        )

        # train model and metrics check on valid sample
        churn_pipe.fit(X_train, y_train)
        metrics_calculation(churn_pipe, X_train, X_val, y_train, y_val)
        joblib.dump(churn_pipe, model_output)
        with open(model_output, "wb") as file:
            pickle.dump(churn_pipe, file)

            # make predictions for test set
        class_ = churn_pipe.predict(X_val)
        pred = churn_pipe.predict_proba(X_val)[:, 1]


        # determine roc-auc and accuracy
        print(f"test roc-auc: {roc_auc_score(y_val, pred)}")
        print(f"test accuracy: {accuracy_score(y_val, class_)}")
        print()
        #print(classification_report(y_val, np.where(pred > 0.5, 1, 0)))
        scores = {'roc_auc':roc_auc_score(y_val, pred),
                  'accuracy':accuracy_score(y_val, class_)}
        signature = infer_signature(X_val, pred)
        #mlflow.log_params()
        mlflow.log_metrics(scores)
        mlflow.sklearn.log_model(churn_pipe,
                                 'model',
                                 signature=signature)
    client = MlflowClient()

    experiment = dict(mlflow.get_experiment_by_name("Default"))

    experiment_id = experiment['experiment_id']
    df = mlflow.search_runs([experiment_id])
    best_run_id = df.loc[0, 'run_id']
    print(best_run_id)

if __name__ == "__main__":
    train_model()
