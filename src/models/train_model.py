# pylint: disable=invalid-name
import pickle
import warnings
from collections import Counter

import click
import pandas as pd
from feature_engine.encoding import OneHotEncoder
from feature_engine.imputation import (
    AddMissingIndicator,
    CategoricalImputer,
    MeanMedianImputer,
)
from sklearn.pipeline import Pipeline
from xgboost import XGBClassifier

from src.models.metrics import metrics_calculation
from src.constants import (
    CATEGORICAL_VARIABLES,
    CATEGORICAL_VARIABLES_WITH_NA,
    NUMERICAL_VARIABLES,
    NUMERIC_VARIABLES_WITH_NA,
    RANDOM_STATE,
    TARGET,
)

warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)


@click.command()
@click.argument("train_data", type=click.Path(exists=True))
@click.argument("validation_data", type=click.Path(exists=True))
@click.argument("model_output", type=click.Path())
def train_model(
    train_data: pd.DataFrame, validation_data: pd.DataFrame, model_output: str
):
    """
    Создание пайплайна, тренировка модели, расчет метрик качества на
    валидационной выборке
    @param train_data:
    @param validation_data:
    @param model_output:
    @return:
    """
    # split data for train and validation
    train_data = pd.read_csv(train_data)
    validation_data = pd.read_csv(validation_data)

    X_train, X_val = (
        train_data.drop(TARGET, axis=1),
        validation_data.drop(TARGET, axis=1),
    )
    y_train, y_val = train_data[TARGET], validation_data[TARGET]
    # count for balance class
    counts = Counter(y_train)
    # set up the pipeline
    churn_pipe = Pipeline(
        [
            # ===== IMPUTATION =====
            # add missing indicator to numerical variables
            (
                "missing_indicator_cat",
                AddMissingIndicator(variables=CATEGORICAL_VARIABLES),
            ),
            # impute categorical variables with string 'missing'
            (
                "categorical_imputation",
                CategoricalImputer(
                    imputation_method="missing", variables=CATEGORICAL_VARIABLES_WITH_NA
                ),
            ),
            # add missing indicator to numerical variables
            (
                "missing_indicator_num",
                AddMissingIndicator(variables=NUMERICAL_VARIABLES),
            ),
            # impute numerical variables with the median
            (
                "median_imputation",
                MeanMedianImputer(
                    imputation_method="median", variables=NUMERIC_VARIABLES_WITH_NA
                ),
            ),
            # == CATEGORICAL ENCODING ======
            # encode categorical variables using one hot encoding into k-1 variables
            ("categorical_encoder", OneHotEncoder(variables=CATEGORICAL_VARIABLES)),
            # estimator
            (
                "xgboost",
                XGBClassifier(
                    nthread=8,
                    scale_pos_weight=counts[0] / counts[1],
                    random_state=RANDOM_STATE,
                ),
            ),
        ]
    )
    # train model and metrics check on valid sample
    churn_pipe.fit(X_train, y_train)
    metrics_calculation(churn_pipe, X_train, X_val, y_train, y_val)

    with open(model_output, "wb") as file:
        pickle.dump(churn_pipe, file)


if __name__ == "__main__":
    train_model()
