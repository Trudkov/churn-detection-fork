# pylint: disable=invalid-name
import numpy as np
from sklearn.metrics import (
    accuracy_score,
    classification_report,
    roc_auc_score,
)


def metrics_calculation(churn_pipe, X_train, X_val, y_train, y_val):
    """
    Расчет метрик roc_auc, accuracy, classification report
    @param churn_pipe: пайплайн
    @param X_train: тренировочный набор данных
    @param X_val: валидационный набор данных
    @param y_train: тренировочные метки оттока
    @param y_val: валидационные метки оттока
    @return: None
    """

    # make predictions for train set
    class_ = churn_pipe.predict(X_train)
    pred = churn_pipe.predict_proba(X_train)[:, 1]
    # determine roc-auc and accuracy

    print(f"train roc-auc: {roc_auc_score(y_train, pred)}")
    print(f"train accuracy: {accuracy_score(y_train, class_)}")
    print()

    # make predictions for test set
    class_ = churn_pipe.predict(X_val)
    pred = churn_pipe.predict_proba(X_val)[:, 1]

    # determine roc-auc and accuracy
    print(f"test roc-auc: {roc_auc_score(y_val, pred)}")
    print(f"test accuracy: {accuracy_score(y_val,  class_)}")
    print()
    print(classification_report(y_val, np.where(pred > 0.5, 1, 0)))
