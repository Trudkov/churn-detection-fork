import pickle
import click
import pandas as pd
from src.constants import TARGET


@click.command()
@click.argument("model_path", type=click.Path(exists=True))
@click.argument("test_data_path", type=click.Path(exists=True))
@click.argument("test_data_output", type=click.Path())
def predict(model_path: str, test_data_path: str, test_data_output: str):
    """
    Предсказания на новом наборе данных
    @param model_path: путь к сохраненной модели
    @param test_data_path: путь к тестовым данным
    @param test_data_output: путь к сохранению данных с предсказанными метками
    @return: None
    """
    with open(model_path, "rb") as file:
        model = pickle.load(file)

    test_data = pd.read_csv(test_data_path)
    test_data[TARGET] = model.predict_proba(test_data)[:1]

    test_data.to_csv(test_data_output, index=False)


if __name__ == "__main__":
    predict()
