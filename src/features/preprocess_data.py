import click
import pandas as pd
from src.constants import TARGET, YES_NO_MAPPER, CAT_COLS_YES_NO_DATA


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.argument("data_type", type=click.STRING)
def preprocess_data(input_path: str, output_path: str, data_type: str):
    """
    Загрузка сохраненных данных с функции Load Data, преобразование данных,
    сохранение преобразованных данных
    @param input_path: путь к данным для загрузки с предыдущего шага
    @param output_path: путь к данным для сохранения после преобразований
    @param data_type: тип данных: train, validation, test
    @return: None
    """
    data = pd.read_csv(input_path)
    # для train/validation есть столбец churn, для test его не таргета не будет
    if data_type != "test":
        data[TARGET] = data[TARGET].map(YES_NO_MAPPER)

    # категориальные колонки точно должны быть категориальными
    for col in CAT_COLS_YES_NO_DATA:
        data[col] = data[col].astype(str)
    # изменение типов для неправильно заданных колонок
    data["SeniorCitizen"] = data["SeniorCitizen"].map({1: "Yes", 0: "No"})
    data["TotalCharges"] = pd.to_numeric(data["TotalCharges"], errors="coerce")

    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    preprocess_data()
