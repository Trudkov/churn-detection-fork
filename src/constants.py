TARGET = "Churn"

RANDOM_STATE = 123

NUMERICAL_VARIABLES = ["tenure", "MonthlyCharges", "TotalCharges"]

NUMERIC_VARIABLES_WITH_NA = ["TotalCharges"]
INDEX_COL = "customerID"
CATEGORICAL_VARIABLES = [
    "customerID",
    "Contract",
    "PaperlessBilling",
    "PaymentMethod",
    "gender",
    "SeniorCitizen",
    "Partner",
    "Dependents",
    "PhoneService",
    "MultipleLines",
    "InternetService",
    "OnlineSecurity",
    "OnlineBackup",
    "DeviceProtection",
    "TechSupport",
    "StreamingTV",
    "StreamingMovies",
]

CATEGORICAL_VARIABLES_WITH_NA = [
    "MultipleLines",
    "OnlineSecurity",
    "OnlineBackup",
    "DeviceProtection",
    "TechSupport",
    "StreamingTV",
    "StreamingMovies",
]

YES_NO_MAPPER = {"Yes": 1, "No": 0}
CAT_COLS_YES_NO_DATA = [
    "Partner",
    "Dependents",
    "PaperlessBilling",
    "OnlineSecurity",
    "OnlineBackup",
    "DeviceProtection",
    "TechSupport",
    "StreamingTV",
    "StreamingMovies",
    "PhoneService",
    "MultipleLines",
]
